<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RecepicesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/recepice/create', [RecepicesController::class, 'create'])->name('recepice.create');
Route::post('/recepice/store', [RecepicesController::class, 'store'])->name('recepice.store');
Route::get('/recepices/detail/{id}', [RecepicesController::class, 'show'])->name('recepice.show');


