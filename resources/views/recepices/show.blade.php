<x-layout>

    <x-masthead></x-masthead>

    <div class="container my-5 contenitore">
        <div class="row">
            <div class="col-12">
                <h1 class="stile-descrizione">{{ $recepice->name }}</h1>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12 col-md-6">
                <img src="{{ Storage::url( $recepice->img ) }}" alt="{{ $recepice->name }}" class="img-fluid">
            </div>
            <div class="col-12 col-md-6">
                <h2 class="stile-descrizione posizione-categoria">Categoria</h2>
                <h4>{{ $recepice->category }}</h4>
                <h2 class="my-3 stile-descrizione">Descrizione</h2>
                <h4>{{ $recepice->description }}</h4>
                <h2 class="my-3 stile-descrizione">Tempo di preparazione</h2>
                <h4>{{ $recepice->preparation_time }}</h4>
                <h2 class="stile-descrizione">Prezzo</h2>
                <h4>€ {{ $recepice->price }}</h4>
            </div>
        </div>
    </div>

</x-layout>