<x-layout>

    <x-masthead></x-masthead>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>Crea una nuova ricetta</h1>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12 col-md-6">
                <x-create-form></x-create-form>
            </div>
        </div>
    </div>
</x-layout>