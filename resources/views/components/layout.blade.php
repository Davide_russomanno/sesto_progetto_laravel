<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Global Recepices</title>
</head>
<body>
    
    <x-nav></x-nav>

    {{ $slot }}

    <div class="spazipoFooter"></div>

    <x-footer></x-footer>

</body>
</html>