<div class="card" style="width: 18rem;">
  <img src="{{ Storage::url($recepice->img) }}" class="card-img-top" alt="{{ $recepice->name }}">
  <div class="card-body">
    <h2 class="card-title">{{ $recepice->category }}</h2>
    <h3 class="card-title my-3">{{ $recepice->name }}</h3>
    <h4 class="card-title my-3">{{ $recepice->description }}</h4>
    <p class="card-title my-3">Tempo di preparazione: {{ $recepice->preparation_time }}</p>
    <p class="card-text my-3">Costo: €{{ $recepice->price }}.00</p>
    <a href="{{ route('recepice.show', $recepice->id) }}" class="btn color-btn">Dettagli</a>
  </div>
</div>