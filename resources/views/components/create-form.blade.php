<form class="stile-form" method="POST" action="{{ route('recepice.store') }}" enctype="multipart/form-data">
    @csrf
  <div class="mb-3">
    <label class="form-label">Categoria della ricetta</label>
    <input type="text" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ old('category') }}">
    @error('category')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Nome della ricetta</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
    @error('name')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Tempo di preparazione della ricetta</label>
    <input type="text" class="form-control @error('preparation_time') is-invalid @enderror" name="preparation_time" value="{{ old('preparation_time') }}">
    @error('preparation_time')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Immagine della ricetta</label>
    <input type="file" class="form-control" name="img">
  </div>
  <div class="mb-3">
    <label class="form-label">Prezzo della ricetta</label>
    <input type="number" step="0.1" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}">
    @error('price')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Descrizione della ricetta</label>
    <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="" cols="30" rows="10">{{ old('description') }}</textarea>
    @error('description')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn color-btn">Crea ricetta</button>
</form>