<nav class="navbar navbar-expand-lg color-nav">
  <div class="container-fluid">
    <a class="navbar-brand stile-logo" href="{{ route('home') }}"><span class="color-g">G</span>LOBAL <span class="color-r">R</span>ECEPICES</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active stile-logo" aria-current="page" href="{{ route('home') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active stile-logo" href="{{ route('recepice.create') }}">Crea la tua ricetta</a>
        </li>
      </ul>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn color-btn" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>