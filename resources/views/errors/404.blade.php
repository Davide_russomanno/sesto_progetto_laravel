<x-layout>


    <div class="container-fluid contenitore-gif">
        <div class="row">
            <div class="col-12 vh-100 d-flex justify-content-center align-items-center">
                <a href="{{ route('home') }}" class="color-btn fs-2 px-5 text-decoration-none text-warning">Torna alla home</a>
            </div>
        </div>
    </div>

</x-layout>