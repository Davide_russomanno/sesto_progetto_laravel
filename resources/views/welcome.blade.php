<x-layout>

    @if (session('message'))
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ session('message') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        </div>
        
    @endif


    <x-masthead></x-masthead>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>Le nostre ricette</h1>
            </div>
        </div>
        <div class="row">
            @if($recepices->isNotEmpty())
                @foreach($recepices as $recepice)
                    <div class="col-12 col-md-3 my-2">
                        <x-recepicecard

                        :recepice="$recepice"
                        
                        ></x-recepicecard>
                    </div>
                @endforeach
            @else
                <div class="col-12 col-md-3 my-4">
                    <h2>Non ci sono ricette</h2>
                </div>
            @endif
        </div>
    </div>


</x-layout>