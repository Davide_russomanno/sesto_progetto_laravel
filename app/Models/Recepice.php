<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recepice extends Model
{
    use HasFactory;

    protected $fillable = [
        'category', 'name', 'description', 'preparation_time', 'img', 'price'
    ];
}
