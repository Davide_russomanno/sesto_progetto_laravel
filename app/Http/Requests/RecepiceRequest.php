<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecepiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [

            'category' => 'required|min:3|max:20',
            'name' => 'required|min:3|max:50',
            'preparation_time' => 'required|min:3|max:30',
            'price' => 'required|numeric|min:1|max:99',
            'description' => 'required|min:7|max:50000'
        ];
    }

    public function messages(){
        return[

            'category.required' => 'La categoria è obbligatoria',
            'category.min' => 'La categoria deve avere almeno 3 caratteri',
            'category.max' => 'La categoria deve avere al massimo 20 caratteri',

            'name.required' => 'Il nome è obbligatorio',
            'name.min' => 'Il nome deve avere almeno 3 caratteri',
            'name.max' => 'Il nome deve avere al massimo 50 caratteri',

            'preparation_time.required' => 'Il tempo di preparazione è obbligatorio',
            'preparation_time.min' => 'Il tempo di preparazione deve avere almeno 3 caratteri',
            'preparation_time.max' => 'Il tempo di preparazione deve avere al massimo 30 caratteri',

            'price.required' => 'Il prezzo è obbligatorio',
            'price.numeric' => 'Il prezzo deve essere numerico',
            'price.min' => 'Il prezzo deve avere almeno 1 carattere',
            'price.max' => 'Il prezzo deve avere al massimo 99 caratteri',

            'description.required' => 'La descrizione è obbligatoria',
            'description.min' => 'La descrizione deve avere almeno 7 caratteri',
            'description.max' => 'La descrizione deve avere al massimo 50000 caratteri',
        ];
    }

    
}
