<?php

namespace App\Http\Controllers;

use App\Models\Recepice;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home() {
        $recepices = Recepice::orderBy('created_at', 'desc')->get();
        return view('welcome', ['recepices' => $recepices]);
    }
}
