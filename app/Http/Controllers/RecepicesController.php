<?php

namespace App\Http\Controllers;

use App\Models\Recepice;
use Illuminate\Http\Request;
use App\Http\Requests\RecepiceRequest;


class RecepicesController extends Controller
{
    public function create(){
        return view('recepices.create');
    }

    public function store(RecepiceRequest $request){
        // category, name, description, preparation_time, img, price

        if($request->file('img') == null){
            $img = "default.png";
        } else{
            $img = $request->file('img')->store('public/recepices');
        }

        Recepice::create(
            [
                'category' => $request->input('category'),
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'preparation_time' => $request->input('preparation_time'),
                'img' => $img,
                'price' => $request->input('price'),
            ]
        );

        return redirect()->route('home')->with('message', 'Grazie per aver caricato una nuova ricetta!');
    }

    public function show($id){

        $recepice = Recepice::findOrFail($id);
        return view('recepices.show', compact('recepice'));
        
    }
}
